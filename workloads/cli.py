import click

from workloads.settings import Settings


@click.group()
def cli():
    pass


settings_option = click.option('--settings', help='Settings module', default='settings')


@cli.command('list')
@settings_option
def list_workloads(settings):
    settings = Settings(settings)
    for wl in settings.workloads:
        print(wl.name)


@cli.command('run')
@settings_option
@click.argument('name')
def run_workload(settings, name):
    settings = Settings(settings)
    workload = settings.get_workload(name)()
    workload.run(queue=settings.queue, worker_count=settings.worker_count)
