import inspect
import threading

import redis


class RedisDataStore(object):
    def __init__(self, connection, namespace):
        self.conn = connection
        self.namespace = namespace
        self.job_queue_name = self.get_job_queue_name(namespace)

    def get(self):
        pass

    def get_job_queue_name(self, namespace):
        return '{}-job-queue'.format(namespace)

    def get_worker_status_key(self, worker):
        return '{}-worker-status-{}'.format(self.namespace, worker)

    def update_worker_status(self, worker_id, status):
        self.conn.set(self.get_worker_status_key(worker_id), status)

    def get_worker_status(self, worker_id):
        self.conn.get(self.get_worker_status_key(worker_id))


class Workload(object):
    name = None

    def __init__(self):
        self.steps = self.discover_steps()
        self.data_store = None
        self.worker_pool = {}

    def run(self, queue, worker_count, *, start_step='start'):
        print("running with queue {}".format(queue))
        print("start step {}".format(start_step))
        self.data_store = self.initialize_data_store(queue)
        print("Using data store {}".format(self.data_store))
        for _ in range(worker_count):
            self.worker_pool[_] = self.create_worker(_)
        self.wait_until_done()

    def discover_steps(self):
        steps = {}
        for attr_name in dir(self):
            attr = getattr(self, attr_name)
            if inspect.ismethod(attr) and hasattr(attr, '__workloads_step__'):
                steps[attr.__workloads_step__] = attr
        return steps

    def create_worker(self, worker_index):
        thr = threading.Thread(target=self._worker, args=(worker_index, self.data_store,))
        thr.start()
        return thr

    def _worker(self, worker_id, data_store):
        data_store.update_worker_status(worker_id, 'idle')
        while True:
            message = data_store.get()

    def initialize_data_store(self, queue_url):
        redis_client = redis.Redis.from_url(queue_url)
        return RedisDataStore(redis_client, namespace=self.name)

    def wait_until_done(self):
        if all(self.data_store.get_worker_status(worker_id) == 'idle' for worker_id in self.worker_pool):
            return


def step(name):
    def decorator(func):
        func.__workloads_step__ = name
        return func

    return decorator


def run_workload(cls, queue):
    workload = cls()
    workload.run(queue)
