import importlib


class Settings:
    def __init__(self, module):
        self.module = importlib.import_module(module)
        self.workloads = self.init_workloads(self.module.WORKLOADS)
        self.queue = self.module.QUEUE
        self.worker_count = self.get_worker_count()

    def init_workloads(self, workload_cls_list):
        workloads = []
        for cls_path in workload_cls_list:
            m, cls = cls_path.rsplit('.', maxsplit=1)
            workloads.append(getattr(importlib.import_module(m), cls))
        return workloads

    def get_workload(self, name):
        for wl in self.workloads:
            if wl.name == name:
                return wl
        raise KeyError("Workload {} not found".format(name))

    def get_worker_count(self, default=8):
        if hasattr(self.module, 'WORKER_COUNT'):
            return self.module.WORKER_COUNT
        else:
            return default
