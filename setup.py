from setuptools import setup, find_packages


def read_file(filename):
    with open(filename, 'r') as f:
        return f.read()


setup(
    name='workloads',
    version='0.0.1',
    description='A python framework for building work load processing scripts.',
    description_long=read_file('README.md'),
    author='Vlad Călin',
    author_email='vlad.s.calin@gmail.com',
    packages=find_packages(exclude=['examples']),
    install_requires=[
        'click',
        'redis',
    ],
    extras_require={
        'dev': [
            'pytest',
            'sphinx',
            'twine',
        ]
    },
    entry_points={
        'console_scripts': [
            'workloads = workloads.cli:cli'
        ]
    }
)
